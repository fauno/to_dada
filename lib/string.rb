# Extiende String para soportar "".to_dada
class String
  # Dadaiza una string
  def to_dada
    # Coja un períodico
    # Coja unas tijeras
    # Escoja en el periódico un artículo de la longitud que cuenta darle
    #   a su poema.
    # Recorte el artículo.
    text = to_s.split
    dada = []

    # Recorte en seguida con cuidado cada una de las palabras que forman
    #   el artículo y métalas en una bolsa.
    # Agítela suavemente.
    until text.size == 0
      line = []

      # Ahora saque cada recorte uno tras otro.
      until rand(2) == 1
        # Copie concienzudamente
        # en el orden en que hayan salido de la bolsa.
        line << text.delete_at(rand(text.size))
      end

      dada << line.join(' ')
    end

    # El poema se parecerá a usted.
    # Y es usted un escritor infinitamente original y de una
    #   sensibilidad hechizante, aunque incomprendida del vulgo.
    dada.join("\n")
  end
end
